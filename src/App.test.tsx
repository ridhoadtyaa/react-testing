import { render, screen } from '@testing-library/react';
import App from './App';

test('should render title page', () => {
	render(<App />);
	const titlePage = screen.getByText(/posts/i);
	expect(titlePage).toBeInTheDocument();
});

// test('renders button card', async () => {
// 	render(<App />);
// 	await new Promise(r => setTimeout(r, 1000));
// 	const buttonCard = screen.getAllByRole('button', {
// 		name: /read more/i,
// 	});
// 	expect(buttonCard).toBeInTheDocument();
// });
