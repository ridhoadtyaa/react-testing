import { Post } from 'posts';

interface PostCardProps {
	post: Post;
}

const PostCard: React.FunctionComponent<PostCardProps> = ({ post }) => {
	return (
		<div className="border border-slate-200 p-3 rounded-md overflow-hidden">
			<img
				src={`https://source.unsplash.com/random/300×30${post.id}`}
				className="w-full object-cover object-center h-32"
				alt={post.title}
				loading="lazy"
			/>
			<div className="mt-4">
				<h3 className="text-lg font-semibold ">{post.title}</h3>
				<button className="text-sm mt-6 font-semibold text-white bg-sky-500 py-1 px-2 rounded-md hover:bg-sky-600 hover:transition hover:duration-300">
					Read More
				</button>
			</div>
		</div>
	);
};

export default PostCard;
