import { render, screen } from '@testing-library/react';
import PostCard from './PostCard';

test('should render a button read more', () => {
	render(<PostCard post={{ userId: 222, id: 1, title: 'Test title', body: 'Test body' }} />);
	const buttonCardElement = screen.getByRole('button', { name: /read more/i });
	expect(buttonCardElement).toBeInTheDocument();
});

test('should render title post card', () => {
	render(<PostCard post={{ userId: 222, id: 1, title: 'Test title', body: 'Test body' }} />);
	expect(screen.getByText('Test title')).toBeInTheDocument();
});
